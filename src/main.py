#!/usr/bin/python3
import socket
import os
import json
import widget_image
from urllib.request import urlopen

with open(__file__, 'r') as f:
    this_file = f.read()
doc = '```python\n' + this_file + '\n```'

def getJson():
    title = 'https://gitlab.com/JoelErni/minimap/-/raw/main/title.gif?ref_type=heads'
    return json.dumps(
            {
                'kind': 'update_widget',
                'widget': {
                    'title': 'MINI MAP SYSTEM OMG GIGA LUL 9000',
                    'width': 4,
                    'height': 2,
                    'content': {
                        "kind": "stack",
                        'content': 
                        [
                            {
                                'kind': 'image',
                                'url': title
                            },
                            {
                                'kind': 'image',
                                'url': widget_image.createImage()
                            },
                            {
                                'kind': 'text',
                                'text': f'X: {widget_image.getPosition()}'
                            },
                            {
                                'kind': 'text',
                                'text': f'Zoom: {widget_image.zoom}x'
                            },
                            {
                                'kind': 'stack',
                                'content': [
                                    {
                                        "kind": "button",
                                        "id": json.dumps({'zoom': 0.1}),
                                        "text": "0.1x",
                                        "pressed": False
                                    },
                                    {
                                        "kind": "button",
                                        "id": json.dumps({'zoom': 0.25}),
                                        "text": "0.25x",
                                        "pressed": False
                                    },
                                    {
                                        "kind": "button",
                                        "id": json.dumps({'zoom': 0.5}),
                                        "text": "0.5x",
                                        "pressed": False
                                    },
                                    {
                                        "kind": "button",
                                        "id": json.dumps({'zoom': 0.75}),
                                        "text": "0.75x",
                                        "pressed": False
                                    },
                                    {
                                        "kind": "button",
                                        "id": json.dumps({'zoom': 1}),
                                        "text": "1x",
                                        "pressed": False
                                    },
                                    {
                                        "kind": "button",
                                        "id": json.dumps({'zoom': 1.5}),
                                        "text": "1.5x",
                                        "pressed": False
                                    },
                                ]
                            },
                        ]
                    }
                }
            }
        ).encode('utf-8')

def setZoom(input):
    if 'id' in input:
        zoom = json.loads(input['id'])['zoom']
        widget_image.zoom = zoom
        print(f'set Zoom to ' + str(zoom))

#https://realpython.com/python-sockets/
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('192.168.0.3', 2002))
    print('connected')
    s.sendall(getJson())
    s.sendall(b'\0') # null-byte -> end of message
    print('100 passed')
    s.sendall(json.dumps({
        'kind': 'update_doc',
        'doc': doc,
    }).encode('utf-8'))
    s.sendall(b'\0') # null-byte -> end of message
    buffer = bytes()
    print('107 passed')
    while True:
        received = s.recv(1)
        if len(received) == 0:
            print('disconnected')
            break
        if received == b'\0':
            print(buffer)
            setZoom(json.loads(buffer.decode()))
            s.sendall(getJson())
            s.sendall(b'\0')
            buffer = bytes()
        else:
            buffer += received