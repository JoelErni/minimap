class Vec:
    x: float
    y: float
    angle: float
    color: str

    def __init__(self, x, y, angle, color=None):
        self.x = x
        self.y = y
        self.angle = angle
        self.color = color