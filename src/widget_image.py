import json
import requests
import base64
from PIL import Image, ImageDraw, ImageFont, ImageColor
import base64
from io import BytesIO
import urllib.request
import math
from vec import Vec

width = 2048
    
stations = {
            'Core Station': Vec(x=0, y=0, angle=0),
            'Vesta Station': Vec(x=10000, y=10000, angle=0),
            'Azura Station': Vec(x=-1000, y=1000, angle=0),
            'Shangris Station': Vec(x=-23542, y=36158, angle=0),
            'Platin Mountain': Vec(x=49600, y=77618, angle=0, color="#6697bd"),
            'Artemis Station': Vec(x=-21000, y=37792, angle=0),
            'Elyse Terminal': Vec(x=-9000, y=-7500, angle=0),
            'Illume Colony': Vec(x=666, y=666, angle=0),
            'Zurro Station': Vec(x=30000, y=-4000, angle=0),
            'Phantom Station': Vec(x=-60000, y=9000, angle=0),
            'Tiberius Station': Vec(x=-40000, y=-20000, angle=0),
            'Nemesis Station': Vec(x=11000, y=-11000, angle=0),
            'Research Station': Vec(x=18000, y=-20000, angle=0),
            'Uran Asteroid': Vec(x=-21500, y=36500, angle=0, color="#64eb34"),
}

zoom = 1.25

def getPosition():
    response = json.loads(requests.get('http://192.168.0.3:2010/pos').content.decode('utf-8'))
    return response['pos']

def convertToLocalPos(pos, game_width):
    output = ((pos['x'] / game_width * width / 2 + width / 2) - getPosition()['x']  / game_width * width / 2,
              (-pos['y'] / game_width * width / 2 + width/2) + getPosition()['y']  / game_width * width / 2,
              pos['angle'])
    return output

def createImage():
    game_width = 0
    for station in stations:
        if abs(stations[station].x) > game_width:
            game_width = abs(stations[station].x)
        if abs(stations[station].y) > game_width:
            game_width = abs(stations[station].y)
    game_width = int(game_width * zoom)

    img  = Image.new( mode = "RGB", size = (width, width) )

    draw = ImageDraw.Draw(img)

    font = ImageFont.truetype("font.ttf", size= int(50))

    #grid
    for x in range(game_width):
        interval = 100
        if x % interval == 0:
            draw.line((-game_width, x, game_width, x), fill='gray', width=1)
            draw.line((x, game_width, x, -game_width ), fill='gray', width=1)

    #Radar radius
    scanner_radius = 2500 / zoom
    draw.ellipse((width / 2 + -scanner_radius / game_width * width, width / 2 + -scanner_radius  / game_width * width, width / 2 + scanner_radius  / game_width * width, width / 2 + scanner_radius  / game_width * width),
                 fill='#114700',
                 outline='green',
                 width=5)

    # Stations
    for station in stations:
        stationRadius = 7.5 / zoom
        global_pos = {"x": stations[station].x, "y":stations[station].y, 'angle': stations[station].angle}
        pos = convertToLocalPos(global_pos, game_width)
        if stations[station].color != None:
            draw.ellipse((pos[0]-stationRadius, pos[1]-stationRadius ,pos[0]+stationRadius, pos[1]+stationRadius), fill=ImageColor.getrgb(stations[station].color))
        else:
            draw.ellipse((pos[0]-stationRadius, pos[1]-stationRadius ,pos[0]+stationRadius, pos[1]+stationRadius), fill=ImageColor.getrgb("#add8e6"))
        
        draw.text((pos[0], pos[1]), str(station), fill='white', font=font)

    # Ship
    pos = (width/2, width/2, getPosition()['angle'])

    t_width = 50
    t_height = 25

    r = -math.radians(pos[2]-90)

    draw.polygon([((t_width * math.cos(r) + 0 * math.sin(r)) + pos[0], (-t_width * math.sin(r) + 0 * math.cos(r)) + pos[1]),
                  ((-t_width * math.cos(r) + t_height * math.sin(r)) + pos[0], (t_width * math.sin(r) + t_height * math.cos(r)) + pos[1]),
                  ((-t_width * math.cos(r) + -t_height * math.sin(r)) + pos[0], (t_width * math.sin(r) + -t_height * math.cos(r)) + pos[1])
                ],
                fill='red')

    buffered = BytesIO()    
    img.save(buffered, format="JPEG")

    return f'data:image/png;base64,{base64.b64encode(buffered.getvalue()).decode()}'