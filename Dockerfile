FROM python:latest
WORKDIR /usr/app/src
COPY ./src/* .
COPY ./requirements.txt /tmp/
RUN pip install --upgrade pip
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt
CMD ["python3", "main.py"]